from django.db import models
from django.utils.translation import gettext_lazy as _


class Shop(models.Model):
    class Meta:
        verbose_name = _('Это модель магазина')

    title = models.CharField(verbose_name=_('это название магазина'), max_length=255)


class Product(models.Model):
    class Meta:
        verbose_name = _('Это модель продукта в магазине')

    title = models.CharField(verbose_name=_('это название продукта'), max_length=255)
    shop = models.ForeignKey(Shop, verbose_name=_('это ссылка на магазин'), on_delete=models.CASCADE)


class Image(models.Model):
    class Meta:
        verbose_name = _('Это картинка продукта')

    src = models.ImageField(verbose_name=_('это файл картинки'))
    product = models.ForeignKey(Product, verbose_name=_('это ссылка на продукт'), on_delete=models.CASCADE)
