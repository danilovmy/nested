from django.contrib.admin.options import ModelAdmin, TabularInline, StackedInline
from django.utils.translation import gettext_lazy as _
from django.template.loader import get_template
from django.utils.functional import cached_property
from .models import Image, Product


class ImageAdminInline(TabularInline):
    extra = 1
    model = Image


class ProductModelAdmin(ModelAdmin):
    inlines = ImageAdminInline,
    fields = 'image_inline', 'title',
    readonly_fields= 'image_inline',

    def image_inline(self, *args, **kwargs):
        context = getattr(self.response, 'context_data', None) or {} # somtimes context.copy() is better
        inline = context['inline_admin_formset'] = context['inline_admin_formsets'].pop(0)
        return get_template(inline.opts.template).render(context, self.request)

    def render_change_form(self, request, *args, **kwargs):
        self.request = request
        self.response = super().render_change_form(request, *args, **kwargs)
        return self.response


class MyForm(StackedInline.form):
    """docstring for MyForm"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instance.form = self

    def is_valid(self):
        return super().is_valid() and self.nested.formset.is_valid()


    @cached_property
    def nested(self):
        modeladmin = ProductModelAdmin(self._meta.model, self.modeladmin.admin_site)
        formsets, instances = modeladmin._create_formsets(self.modeladmin.request, self.instance, change=self.instance.pk)
        inline = modeladmin.get_inline_formsets(self.modeladmin.request, formsets[:1], instances[:1], self.instance)[0]
        inline.formset.prefix = f'{self.prefix}_{formsets[0].prefix}'.replace('-', '_')
        return inline

    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs) or self.nested.formset.save(*args, **kwargs)


class ProductInline(StackedInline):
    model = Product
    fields = 'title', 'image_inline'
    readonly_fields = 'image_inline',
    extra = 1
    form = MyForm

    def image_inline(self, obj=None, *args, **kwargs):
        context = getattr(self.modeladmin.response, 'context_data', None) or {}
        return get_template(obj.form.nested.opts.template).render(context | {'inline_admin_formset': obj.form.nested}, self.modeladmin.request)

    def get_formset(self, *args, **kwargs):
        formset = super().get_formset(*args, **kwargs)
        formset.form.modeladmin = self.modeladmin
        return formset


class ShopModelAdmin(ModelAdmin):
    inlines = ProductInline,
    fields = 'title',

    def render_change_form(self, request, *args, **kwargs):
        self.request = request
        response = self.response = super().render_change_form(request, *args, **kwargs)
        return response

    def get_inline_instances(self, *args, **kwargs):
        yield from ((inline, vars(inline).update(modeladmin=self))[0] for inline in super().get_inline_instances(*args, **kwargs))
