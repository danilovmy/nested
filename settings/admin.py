from django.contrib.admin.apps import AdminConfig as BaseConfig
from django.apps import apps
from django.contrib.admin.sites import site


class AdminsConfig(BaseConfig):

    def ready(self, *args, **kwargs):
        super().ready(*args, **kwargs)
        for config in apps.get_app_configs():
            admin_module = getattr(config.module, 'admin', None)
            for model in config.get_models():
                self.register_modeladmin(model, admin_module, site)

    def register_modeladmin(self, model, admin, adminsite):
        if not adminsite.is_registered(model):
            modeladmin = getattr(admin, f'{model.__name__}ModelAdmin', None)
            adminsite.register(model, modeladmin)

